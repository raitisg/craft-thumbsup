<?php

namespace raitisg\thumbsup\elements\db;

use craft\elements\db\ElementQuery;

class VoteQuery extends ElementQuery
{
	private string $rnd_table_name;

	/**
	 * @inheritdoc
	 */
	public function __construct($elementType, array $config = []) {
		$this->rnd_table_name = 'elements_' . mt_rand();

		$this->join('LEFT JOIN', 'elements AS ' . $this->rnd_table_name, $this->rnd_table_name . '.revisionId = thumbsup_totals.key');
		$this->join('LEFT JOIN', 'revisions', 'revisions.id = thumbsup_totals.key');

		parent::__construct($elementType, $config);
	}

	/**
	 * @inheritdoc
	 */
	protected function beforePrepare(): bool {
		$this->joinElementTable('thumbsup_totals');

		$this->query->addSelect([
			'thumbsup_totals.key',
			'thumbsup_totals.tally',
			'thumbsup_totals.total',
			'thumbsup_totals.upvotes',
			'thumbsup_totals.downvotes',
			$this->rnd_table_name . '.dateCreated AS postDate',
			'revisions.num AS revision',
		]);

		// This will sort correctly, but disable native table sorting:
		//$this->orderBy([
		//	'content.title' => SORT_ASC,
		//	'revisions.num' => SORT_DESC,
		//]);

		$this->groupBy(['thumbsup_totals.id', 'thumbsup_totals.key', 'revisions.num']);

		return parent::beforePrepare();
	}
}
