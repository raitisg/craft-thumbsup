<?php

namespace raitisg\thumbsup\elements;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQueryInterface;
use craft\helpers\UrlHelper;
use raitisg\thumbsup\elements\db\VoteQuery;
use raitisg\thumbsup\exporters\VotesExporter;
use yii\base\InvalidConfigException;

class Vote extends Element
{
	/**
	 * @var string|null
	 */
	public $key;

	/**
	 * @var int
	 */
	public $upvotes;

	/**
	 * @var int
	 */
	public $downvotes;

	/**
	 * @var int
	 */
	public $total;

	/**
	 * @var int
	 */
	public $tally;

	/**
	 * @var string
	 */
	public $postDate;

	/**
	 * @var int
	 */
	public $revision;

	//public static function displayName(): string
	//{
	//    return Craft::t('thumbsup', 'Votes');
	//}

	//public static function refHandle()
	//{
	//    return 'vote';
	//}

	/**
	 * @return bool
	 */
	public static function hasContent(): bool {
		return true;
	}

	/**
	 * @return bool
	 */
	public static function hasTitles(): bool {
		return true;
	}

	/**
	 * @return ElementQueryInterface
	 */
	public static function find(): ElementQueryInterface {
		return new VoteQuery(static::class);
	}

	/**
	 * @param string|null $context
	 *
	 * @return array|array[]
	 */
	protected static function defineSources(string $context = null): array {
		if ($context === 'index') {
			return [[
				'key' => '*',
				'label' => Craft::t('thumbsup', 'All votes'),
				'criteria' => []
			]];
		}

		return [];
	}

	/**
	 * @return string
	 * @throws InvalidConfigException
	 */
	public function getCpEditUrl(): string {
		$url = UrlHelper::cpUrl(
			'entries/faq/'.$this->id.'?revisionId='.$this->key
		);

		if (Craft::$app->isMultiSite) {
			$url .= '&site='.$this->getSite()->handle;
		}

		return $url;
	}

	/**
	 * @return array[]
	 */
	protected static function defineSortOptions(): array {
		return [
			'title' => Craft::t('thumbsup', 'Title'),
			'total' => Craft::t('thumbsup', 'Total'),
			'upvotes' => Craft::t('thumbsup', 'Upvotes'),
			'downvotes' => Craft::t('thumbsup', 'Downvotes'),
			'tally' => Craft::t('thumbsup', 'Tally'),
		];
	}

	/**
	 * @return array[]
	 */
	protected static function defineTableAttributes(): array {
		return [
			'title' => ['label' => Craft::t('thumbsup', 'Title')],
			'revision' => ['label' => Craft::t('thumbsup', 'Revision')],
			'postDate' => ['label' => Craft::t('thumbsup', 'Post Date')],
			'total' => ['label' => Craft::t('thumbsup', 'Total')],
			'upvotes' => ['label' => Craft::t('thumbsup', 'Upvotes')],
			'downvotes' => ['label' => Craft::t('thumbsup', 'Downvotes')],
			'tally' => ['label' => Craft::t('thumbsup', 'Tally')],
		];
	}

	/**
	 * @param string $source
	 *
	 * @return array
	 */
	protected static function defineExporters(string $source): array {
		$exporters = parent::defineExporters($source);

		array_unshift($exporters, VotesExporter::class);

		return $exporters;
	}
}
