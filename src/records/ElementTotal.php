<?php

namespace raitisg\thumbsup\records;

use craft\db\ActiveRecord;

class ElementTotal extends ActiveRecord
{
	public static function tableName(): string {
		return '{{%thumbsup_totals}}';
	}
}
