<?php

namespace raitisg\thumbsup\records;

use craft\db\ActiveRecord;

class ElementVote extends ActiveRecord
{
	public static function tableName(): string {
		return '{{%thumbsup_votes}}';
	}
}
