<?php

namespace raitisg\thumbsup\variables;

use Craft;
use craft\elements\db\ElementQuery;
use craft\elements\Entry;
use craft\helpers\Template;
use craft\helpers\UrlHelper;
use raitisg\thumbsup\ThumbsUp;
use raitisg\thumbsup\web\assets\CssAssets;
use raitisg\thumbsup\web\assets\JsAssets;
use Twig\Markup;
use yii\base\InvalidConfigException;

class ThumbsUpVariable
{
	/**
	 * @var bool
	 */
	private $is_css_included = false;

	/**
	 * @var bool
	 */
	private $is_js_included = false;

	/**
	 * @param int $element_id
	 * @param int $key
	 * @param string|null $icon
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function upvote(int $element_id, int $key, string $icon = null): Markup {
		if ($icon) {
			ThumbsUp::$plugin->thumbsup_vote->upvoteIcon = $icon;
		}

		return $this->_renderIcon(ThumbsUp::UPVOTE, $element_id, $key);
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 * @param string|null $icon
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function downvote(int $element_id, int $key, string $icon = null): Markup {
		if ($icon) {
			ThumbsUp::$plugin->thumbsup_vote->downvoteIcon = $icon;
		}

		return $this->_renderIcon(ThumbsUp::DOWNVOTE, $element_id, $key);
	}

	/**
	 * @param Entry $entry
	 *
	 * @return int
	 */
	public function revision(Entry $entry): int {
		return $entry->getCurrentRevision()->revisionId;
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function tally(int $element_id, int $key): Markup {
		return $this->_renderNumber('thumbsup-tally', $element_id, $key);
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function totalVotes(int $element_id, int $key): Markup {
		return $this->_renderNumber('thumbsup-total-votes', $element_id, $key);
	}

	/**
	 * Output total upvotes of element
	 *
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function totalUpvotes(int $element_id, int $key): Markup {
		return $this->_renderNumber('thumbsup-total-upvotes', $element_id, $key);
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	public function totalDownvotes(int $element_id, int $key): Markup {
		return $this->_renderNumber('thumbsup-total-downvotes', $element_id, $key);
	}

	/**
	 * @param string $class
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	private function _renderNumber(string $class, int $element_id, ?string $key): Markup {
		$this->_includeJs();

		$upvote = ThumbsUp::$plugin->thumbsup;

		$genericClass = 'thumbsup-element '.$class;
		$uniqueClass = $class.'-'.$upvote->setItemKey($element_id, $key, '-');
		$class = $genericClass.' '.$uniqueClass;

		$id = $upvote->setItemKey($element_id, $key);

		return Template::raw('<span data-id="'.$id.'" class="'.$class.'">&nbsp;</span>');
	}

	/**
	 * @param int $vote
	 * @param int $element_id
	 * @param int $key
	 *
	 * @return Markup
	 * @throws InvalidConfigException
	 */
	private function _renderIcon(int $vote, int $element_id, int $key): Markup {
		$this->_includeCss();

		$upvote = ThumbsUp::$plugin->thumbsup;

		$genericClass = 'thumbsup-element ';
		$js = '';
		$icon = '';
		$uniqueClass = '';
		switch ($vote) {
			case ThumbsUp::UPVOTE:
				$icon = ThumbsUp::$plugin->thumbsup_vote->upvoteIcon;
				$js = $this->jsUpvote($element_id, $key);
				$genericClass .= 'thumbsup-upvote';
				$uniqueClass = 'thumbsup-upvote-'.$upvote->setItemKey($element_id, $key, '-');
				break;
			case ThumbsUp::DOWNVOTE:
				$icon = ThumbsUp::$plugin->thumbsup_vote->downvoteIcon;
				$js = $this->jsDownvote($element_id, $key);
				$genericClass .= 'thumbsup-downvote';
				$uniqueClass = 'thumbsup-downvote-'.$upvote->setItemKey($element_id, $key, '-');
				break;
		}

		$dataId = $upvote->setItemKey($element_id, $key);

		$span = '<span data-id="'.$dataId.'" class="'.$genericClass.' '.$uniqueClass.'" onclick="'.$js.'">'.$icon.'</span>';

		return Template::raw($span);
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 * @param string|null $prefix
	 *
	 * @return string
	 * @throws InvalidConfigException
	 */
	public function jsUpvote(int $element_id, int $key, string $prefix = null): string {
		$this->_includeJs();

		return ($prefix ? 'javascript:' : '')."thumbsup.upvote($element_id, $key)";
	}

	/**
	 * @param int $element_id
	 * @param int $key
	 * @param string|null $prefix
	 *
	 * @return string
	 * @throws InvalidConfigException
	 */
	public function jsDownvote(int $element_id, int $key, string $prefix = null): string {
		$this->_includeJs();

		return ($prefix ? 'javascript:' : '')."thumbsup.downvote($element_id, $key)";
	}

	/**
	 * @return void
	 * @throws InvalidConfigException
	 */
	private function _includeCss(): void {
		if ($this->is_css_included) {
			return;
		}

		$view = Craft::$app->getView();

		$view->registerAssetBundle(CssAssets::class);

		$this->is_css_included = true;
	}

	/**
	 * @return void
	 * @throws InvalidConfigException
	 */
	private function _includeJs(): void {
		if ($this->is_js_included) {
			return;
		}

		$view = Craft::$app->getView();

		$view->registerAssetBundle(JsAssets::class);

		if (Craft::$app->getConfig()->getGeneral()->devMode) {
			$view->registerJs('thumbsup.devMode = true;', $view::POS_END);
		}

		$view->registerJs('thumbsup.actionUrl = "'.UrlHelper::actionUrl().'";', $view::POS_END);

		$this->is_js_included = true;
	}

	/**
	 * @param array $icons
	 */
	public function setIcons(array $icons): void {
		ThumbsUp::$plugin->thumbsup_vote->setIcons($icons);
	}

	/**
	 * Sort by "highest rated"
	 *
	 * @param ElementQuery $elements
	 * @param int $key
	 */
	public function sort(ElementQuery $elements, int $key): void {
		ThumbsUp::$plugin->thumbsup_query->orderByTally($elements, $key);
	}
}
