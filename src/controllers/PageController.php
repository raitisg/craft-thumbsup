<?php

namespace raitisg\thumbsup\controllers;

use Craft;
use craft\web\Controller;
use raitisg\thumbsup\ThumbsUp;
use yii\web\Response;

class PageController extends Controller
{
	protected array|int|bool $allowAnonymous = true;

	/**
	 * @return Response
	 */
	public function actionPreload(): Response {
		$preload = (bool)ThumbsUp::$plugin->getSettings()->preload;

		return $this->asJson(['enabled' => $preload]);
	}

	/**
	 * @return Response
	 */
	public function actionCsrf(): Response {
		$request = Craft::$app->getRequest();

		return $this->asJson([$request->csrfParam => $request->getCsrfToken()]);
	}

	/**
	 * @return Response
	 */
	public function actionConfigure(): Response {
		$data = [];

		$ids = Craft::$app->getRequest()->getBodyParam('ids[]');

		if (!is_array($ids) || empty($ids)) {
			return $this->asJson('Invalid IDs, unable to configure Upvote elements.');
		}

		foreach ($ids as $itemKey) {
			$data[] = ThumbsUp::$plugin->thumbsup->compileElementData($itemKey);
		}

		return $this->asJson($data);
	}
}
