<?php

namespace raitisg\thumbsup\controllers;

use Craft;
use craft\web\Controller;
use raitisg\thumbsup\ThumbsUp;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class VoteController extends Controller
{
	protected array|int|bool $allowAnonymous = true;

	/**
	 * @throws BadRequestHttpException
	 */
	public function actionUpvote(): Response {
		$this->requirePostRequest();

		return $this->_castVote(ThumbsUp::UPVOTE);
	}

	/**
	 * @throws BadRequestHttpException
	 */
	public function actionDownvote(): Response {
		$this->requirePostRequest();

		$settings = ThumbsUp::$plugin->getSettings();

		if (!$settings->allowDownvoting) {
			return $this->asJson('Downvoting is disabled.');
		}

		return $this->_castVote(ThumbsUp::DOWNVOTE);
	}

	/**
	 * @throws BadRequestHttpException
	 */
	public function actionSwap(): Response {
		$this->requirePostRequest();

		$settings = ThumbsUp::$plugin->getSettings();

		if (!$settings->allowVoteRemoval) {
			return $this->asJson('Unable to swap vote. Vote removal is disabled.');
		}

		if (!$settings->allowDownvoting) {
			return $this->asJson('Unable to swap vote. Downvoting is disabled.');
		}

		$request = Craft::$app->getRequest();

		$elementId = $request->getBodyParam('id');
		$key = $request->getBodyParam('key');

		$response = ThumbsUp::$plugin->thumbsup_vote->removeVote($elementId, $key);

		if (!is_array($response)) {
			return $this->asJson($response);
		}

		return $this->_castVote($response['userVote']);
	}

	/**
	 * @param int $vote
	 *
	 * @return Response
	 */
	private function _castVote(int $vote): Response {
		$request = Craft::$app->getRequest();

		$elementId = $request->getBodyParam('id');
		$key = $request->getBodyParam('key');

		$response = ThumbsUp::$plugin->thumbsup_vote->castVote($elementId, $key, $vote);

		return $this->asJson($response);
	}

	/**
	 * @throws BadRequestHttpException
	 */
	public function actionRemove(): Response {
		$this->requirePostRequest();

		$request = Craft::$app->getRequest();

		$elementId = $request->getBodyParam('id');
		$key = $request->getBodyParam('key');

		$response = ThumbsUp::$plugin->thumbsup_vote->removeVote($elementId, $key);
		ThumbsUp::$plugin->thumbsup_vote->removeVoteFromArchive($elementId, $key);

		return $this->asJson($response);
	}
}
