<?php

namespace raitisg\thumbsup\services;

use Craft;
use craft\base\Component;
use craft\helpers\Json;
use raitisg\thumbsup\records\ElementTotal;
use raitisg\thumbsup\records\ElementVote;
use raitisg\thumbsup\ThumbsUp;
use yii\web\Cookie;

class Vote extends Component
{
	public $upvoteIcon = '▲';
	public $downvoteIcon = '▼';
	public $alreadyVoted = 'Already voted.';

	/**
	 * @return void
	 */
	public function init(): void {
	}

	/**
	 * @param array $icons
	 *
	 * @return void
	 */
	public function setIcons(array $icons): void {
		[$this->upvoteIcon, $this->downvoteIcon] = $icons;
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 * @param int $vote
	 *
	 * @return array|string
	 */
	public function castVote(int $elementId, int $key, int $vote) {
		// Prep return data
		$itemKey = ThumbsUp::$plugin->thumbsup->setItemKey($elementId, $key);
		$returnData = ThumbsUp::$plugin->thumbsup->compileElementData($itemKey, $vote);

		// Update original history
		ThumbsUp::$plugin->thumbsup->history[$itemKey] = $vote;

		if (!$this->_updateElementVoteCookie($elementId, $key, $vote)) {
			return $this->alreadyVoted;
		}

		$this->_updateElementTotals($elementId, $key, $vote);

		$this->saveVoteToArchive($elementId, $key, $vote);

		return $returnData;
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return array|string
	 */
	public function removeVote(int $elementId, int $key): array {
		// Prep return data
		$itemKey = ThumbsUp::$plugin->thumbsup->setItemKey($elementId, $key);
		$returnData = ThumbsUp::$plugin->thumbsup->compileElementData($itemKey, null, true);

		// Get original vote
		$originalVote = $returnData['userVote'];

		// If no original vote, bail
		if (!$originalVote) {
			return 'Unable to remove vote. No vote was ever cast.';
		}

		// Get antivote
		$antivote = (-1 * $originalVote);
		$returnData['userVote'] = $antivote;

		$this->_removeVoteFromCookie($elementId, $key);

		$this->_updateElementTotals($elementId, $key, $antivote, true);

		unset(ThumbsUp::$plugin->thumbsup->history[$itemKey]);

		return $returnData;
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 * @param int $vote
	 *
	 * @return bool
	 */
	private function _updateElementVoteCookie(int $elementId, int $key, int $vote): bool {
		// Get anonymous history
		$history =& ThumbsUp::$plugin->thumbsup->votes;
		$item = ThumbsUp::$plugin->thumbsup->setItemKey($elementId, $key);
		// Cast vote
		$history[$item] = $vote;
		$this->saveElementVoteCookie();

		return true;
	}

	/**
	 * @return void
	 */
	public function saveElementVoteCookie(): void {
		// Get cookie settings
		$cookieName = ThumbsUp::$plugin->thumbsup->cookie['name'];
		$history = ThumbsUp::$plugin->thumbsup->votes;
		$lifespan = ThumbsUp::$plugin->thumbsup->cookie['ttl'];
		// Set cookie
		$cookie = new Cookie();
		$cookie->name = $cookieName;
		$cookie->value = Json::encode($history);
		$cookie->expire = time() + $lifespan;
		Craft::$app->getResponse()->getCookies()->add($cookie);
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 * @param int $vote
	 * @param bool $antivote
	 *
	 * @return void
	 */
	private function _updateElementTotals(int $elementId, int $key, int $vote, bool $antivote = false): void {
		$key = $key ?? $elementId;

		// Load existing element totals
		$record = ElementTotal::findOne([
			'id' => $elementId,
			'key' => $key,
		]);

		// If no totals record exists, create new
		if (!$record) {
			$record = new ElementTotal;
			$record->id = $elementId;
			$record->key = $key;
			$record->upvotes = 0;
			$record->downvotes = 0;
		}

		// If vote is being removed
		if ($antivote) {
			// Register unvote (default behavior)
			switch ($vote) {
				case  1:
					$record->downvotes--;
					break;
				case -1:
					$record->upvotes--;
					break;
			}
		} else {
			// Register vote
			switch ($vote) {
				case  1:
					$record->upvotes++;
					break;
				case -1:
					$record->downvotes++;
					break;
			}
		}

		$record->total = $record->upvotes + $record->downvotes;
		$record->tally = $record->upvotes - $record->downvotes;

		$record->save();
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return void
	 */
	private function _removeVoteFromCookie(int $elementId, int $key): void {
		$votes =& ThumbsUp::$plugin->thumbsup->votes;

		if (!$votes) {
			return;
		}

		$item = ThumbsUp::$plugin->thumbsup->setItemKey($elementId, $key);

		if (!isset($votes[$item])) {
			return;
		}

		unset($votes[$item]);
		$this->saveElementVoteCookie();
	}

	/**
	 * @return string
	 */
	public function getUserHash(): string {
		$request = Craft::$app->request;

		return sha1($request->getUserIp().'|'.$request->getUserAgent());
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 * @param int $vote
	 *
	 * @return void
	 */
	public function saveVoteToArchive(int $elementId, int $key, int $vote): void {
		$record = new ElementVote();
		$record->id = $elementId;
		$record->key = $key;
		$record->vote = $vote;
		$record->userHash = $this->getUserHash();
		$record->save();
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return void
	 */
	public function removeVoteFromArchive(int $elementId, int $key): void {
		ElementVote::deleteAll('`id` = :id AND `key` = :key AND `userHash` = :userHash', [
			'id' => $elementId,
			'key' => $key,
			'userHash' => $this->getUserHash()
		]);
	}
}
