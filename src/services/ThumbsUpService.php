<?php

namespace raitisg\thumbsup\services;

use Craft;
use craft\base\Component;
use craft\helpers\Json;
use raitisg\thumbsup\ThumbsUp;

class ThumbsUpService extends Component
{
	/**
	 * @var
	 */
	//public $settings;

	/**
	 * @var array
	 */
	public $votes = [];

	/**
	 * @var array
	 */
	public $history = [];

	/**
	 * @var array
	 */
	public $cookie = [
		'name' => 'VoteHistory',
		'ttl' => 315569260 // 10 years
	];

	/**
	 * @return void
	 */
	public function init(): void {
		$this->history =& $this->votes;

		parent::init();
	}

	/**
	 * Generate combined item key
	 *
	 * @param int $elementId
	 * @param int $key
	 * @param string $separator
	 *
	 * @return string
	 */
	public function setItemKey(int $elementId, int $key, string $separator = ':'): string {
		return $elementId.$separator.$key;
	}

	/**
	 * Get history of anonymous user
	 *
	 * @return void
	 */
	public function getVotes(): void {
		// Get request
		$request = Craft::$app->getRequest();

		// If running via command line, bail
		if ($request->getIsConsoleRequest()) {
			return;
		}

		// Get cookies object
		$cookies = $request->getCookies();

		// If cookie exists
		if ($cookies->has($this->cookie['name'])) {
			// Get history from cookie
			$cookie_value = $cookies->getValue($this->cookie['name']);
			$this->votes = (array)Json::decode($cookie_value, true);
		} else {
			// Initialize history and set cookie
			$this->votes = [];
			ThumbsUp::$plugin->thumbsup_vote->saveElementVoteCookie();
		}
	}

	/**
	 * @param string $itemKey
	 * @param int|null $userVote
	 * @param bool $isAntivote
	 *
	 * @return array
	 */
	public function compileElementData(string $itemKey, int $userVote = null, bool $isAntivote = false): array {
		// Get current user
		$currentUser = Craft::$app->user->getIdentity();

		// Split ID into array
		$parts = explode(':', $itemKey);

		// Get the element ID
		$elementId = (int)array_shift($parts);

		// If no element ID, bail
		if (!$elementId) {
			return [];
		}

		// Reassemble the remaining parts (in case the key contains a colon)
		$key = implode(':', $parts);

		// If no key, set to null
		if (!$key) {
			$key = null;
		}

		// Get user's vote history for this item
		$itemHistory = ($this->history[$itemKey] ?? null);

		// Set vote configuration
		$vote = [
			'id' => $elementId,
			'key' => $key,
			'itemKey' => $itemKey,
			'userId' => ($currentUser ? (int)$currentUser->id : null),
			'userVote' => ($userVote ?? $itemHistory),
			'isAntivote' => $isAntivote,
		];

		// Get element totals from BEFORE the vote is calculated
		$totals = [
			'tally' => ThumbsUp::$plugin->thumbsup_query->tally($elementId, $key),
			'totalVotes' => ThumbsUp::$plugin->thumbsup_query->totalVotes($elementId, $key),
			'totalUpvotes' => ThumbsUp::$plugin->thumbsup_query->totalUpvotes($elementId, $key),
			'totalDownvotes' => ThumbsUp::$plugin->thumbsup_query->totalDownvotes($elementId, $key),
		];

		// If existing vote was removed
		if ($isAntivote && $itemHistory) {
			// Create antivote
			$userVote = $itemHistory * -1;
			// Set total type
			$totalType = (1 === $userVote ? 'totalDownvotes' : 'totalUpvotes');
		} else {
			// Set total type
			$totalType = (1 === $userVote ? 'totalUpvotes' : 'totalDownvotes');
		}

		// If a vote was cast or removed
		if ($userVote) {

			// Add to tally
			$totals['tally'] += $userVote;

			// If removing vote
			if ($isAntivote) {
				// One less vote
				$totals['totalVotes']--;
				$totals[$totalType]--;
			} else {
				// One more vote
				$totals['totalVotes']++;
				$totals[$totalType]++;
			}
		}

		// Return element's vote data
		return array_merge($vote, $totals);
	}
}
