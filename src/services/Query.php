<?php

namespace raitisg\thumbsup\services;

use craft\base\Component;
use craft\elements\db\ElementQuery;
use raitisg\thumbsup\records\ElementTotal;
use yii\db\Expression;

class Query extends Component
{
	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return int
	 */
	public function tally(int $elementId, int $key): int {
		$record = ElementTotal::findOne([
			'id' => $elementId,
			'key' => $key,
		]);

		if (!$record) {
			return 0;
		}

		// Calculate and return total
		return $record->upvotes - $record->downvotes;
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return int
	 */
	public function totalVotes(int $elementId, int $key): int {
		$record = $this->_getRecord($elementId, $key);

		return ($record ? ($record->upvotes + $record->downvotes) : 0);
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return int
	 */
	public function totalUpvotes(int $elementId, int $key): int {
		$record = $this->_getRecord($elementId, $key);

		return ($record->upvotes ?? 0);
	}

	/**
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return int
	 */
	public function totalDownvotes(int $elementId, int $key): int {
		$record = $this->_getRecord($elementId, $key);

		return ($record->downvotes ?? 0);
	}

	/**
	 * Get matching totals record
	 *
	 * @param int $elementId
	 * @param int $key
	 *
	 * @return ElementTotal|null
	 */
	private function _getRecord(int $elementId, int $key): ?ElementTotal {
		return ElementTotal::findOne([
			'id' => $elementId,
			'key' => $key,
		]);
	}

	/**
	 * @param ElementQuery $query
	 * @param int $key
	 *
	 * @return void
	 */
	public function orderByTally(ElementQuery $query, int $key): void {
		// Collect and sort elementIds
		$elementIds = $this->_elementIdsByTally($key);

		// If no element IDs, bail
		if (!$elementIds) {
			return;
		}

		// Match order to elementIds
		$ids = implode(', ', $elementIds);
		$query->orderBy = [new Expression("field([[elements.id]], ".$ids.") desc")];
	}

	/**
	 * @param int $key
	 *
	 * @return array
	 */
	private function _elementIdsByTally(int $key): array {
		$conditions = ['[[totals.key]]' => $key];

		// Construct order SQL
		$upvotes = 'IFNULL([[totals.upvotes]], 0)';
		$downvotes = 'IFNULL([[totals.downvotes]], 0)';
		$order = "(".$upvotes." - ".$downvotes.") DESC, [[elements.id]] DESC";

		// Join with elements table to sort by tally
		$elementIds = (new \craft\db\Query())
			->select('[[elements.id]]')
			->from('{{%elements}} elements')
			->where($conditions)
			->leftJoin('{{%thumbsup_totals}} totals', '[[elements.id]] = [[totals.id]]')
			->orderBy([new Expression($order)])
			->column();

		return array_reverse($elementIds);
	}
}
