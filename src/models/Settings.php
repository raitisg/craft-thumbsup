<?php

namespace raitisg\thumbsup\models;

use craft\base\Model;

class Settings extends Model
{
	/** @var bool $preload Whether to preload data into DOM elements. */
	public $preload = true;

	/** @var bool $allowDownvoting Whether it's possible to downvote. */
	public $allowDownvoting = true;

	/** @var bool $allowVoteRemoval Whether users are allowed to remove their vote. */
	public $allowVoteRemoval = true;
}
