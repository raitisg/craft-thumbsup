<?php

namespace raitisg\thumbsup\web\assets;

use craft\web\AssetBundle;

class CssAssets extends AssetBundle
{
	public function init(): void {
		parent::init();

		$this->sourcePath = '@raitisg/thumbsup/resources';

		$this->css = [
			'css/thumbsup.css',
		];
	}
}