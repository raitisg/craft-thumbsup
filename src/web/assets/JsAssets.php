<?php

namespace raitisg\thumbsup\web\assets;

use craft\web\AssetBundle;
use raitisg\thumbsup\ThumbsUp;

class JsAssets extends AssetBundle
{
	public function init(): void {
		parent::init();

		$this->sourcePath = '@raitisg/thumbsup/resources';

		$this->js = [
			'js/sizzle.js',
			'js/superagent.js',
			'js/thumbsup.js',
		];

		if (ThumbsUp::$plugin->getSettings()->allowVoteRemoval) {
			$this->js[] = 'js/unvote.js';
		}
	}
}