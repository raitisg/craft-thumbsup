const ajax = window.superagent;

window.thumbsup = {
	// Not in devMode by default
	devMode: false,
	// Default action url
	actionUrl: '/actions/',
	// No CSRF token by default
	csrfToken: false,
	// Whether setup has been completed
	setupComplete: false,
	// Initialize upvote elements on page
	pageSetup: function () {
		// Initialize
		const ids = [];
		let elementId;
		// Get relevant DOM elements
		let elements = document.getElementsByClassName('thumbsup-element');
		elements = Array.prototype.slice.call(elements);
		// Loop through elements
		for (let i in elements) {
			// Get element ID
			elementId = elements[i].dataset.id;
			// If element ID is missing, add it
			if (-1 === ids.indexOf(elementId)) {
				ids.push(elementId);
			}
		}
		// Configure all elements on page
		this.configure(ids);
		// Mark setup as complete!
		this.setupComplete = true;
	},
	// Configure elements
	configure: function (ids) {
		// Make object available to callback
		const that = this;
		// Callback function for casting a vote
		const configureElements = function () {
			// Initialize data with CSRF token
			const data = JSON.parse(JSON.stringify(that.csrfToken));
			// Set data
			data['ids[]'] = ids;
			// Remove vote
			ajax
				.post(that.getActionUrl() + '/thumbsup/page/configure')
				.send(data)
				.end(function (err, res) {
					// If something went wrong, bail
					if (!res.ok) {
						console.warn('Error configuring ThumbsUp elements:', err);
						return;
					}
					// Get response data
					const data = res.body;
					// If no elements to configure, bail
					if (!data || !Array.isArray(data)) {
						return;
					}
					// Declare variables for loop
					let entry, group, upvotes, downvotes;
					// Loop through response data
					for (let i in data) {
						// Get entry data
						entry = data[i];
						// Collect matching DOM elements
						group = "[data-id='" + entry['itemKey'] + "']";
						upvotes = document.querySelectorAll(group + ".thumbsup-upvote");
						downvotes = document.querySelectorAll(group + ".thumbsup-downvote");
						// Mark upvote & downvote icons
						switch (parseInt(entry['userVote'])) {
							case 1:
								// Mark upvote
								that._addMatchClass(upvotes);
								break;
							case -1:
								// Mark downvote
								that._addMatchClass(downvotes);
								break;
						}
						// Set all values
						thumbsup._setAllValues(entry);
					}
				})
			;
		};
		// If token already exists
		if (this.csrfToken) {
			// Configure DOM elements using existing token
			configureElements();
		} else {
			// Configure DOM elements using a fresh token
			this.getCsrf(configureElements);
		}
	},
	// Cast an upvote
	upvote: function (elementId, key) {
		if (this.devMode) {
			console.info('[' + elementId + ']' + (key ? ' [' + key + ']' : '') + ' Upvoting...');
		}
		this._vote(elementId, key, 'upvote');
	},
	// Cast a downvote
	downvote: function (elementId, key) {
		if (this.devMode) {
			console.info('[' + elementId + ']' + (key ? ' [' + key + ']' : '') + ' Downvoting...');
		}
		this._vote(elementId, key, 'downvote');
	},
	// Remove vote
	removeVote: function () {
		console.info('Vote removal is disabled.');
	},
	// Normalize action URL
	getActionUrl: function () {
		return this.actionUrl.replace(/\/+$/, '');
	},
	// Submit AJAX with fresh CSRF token
	getCsrf: function (callback) {
		// Fetch a new CSRF token
		ajax
			.get(this.getActionUrl() + '/thumbsup/page/csrf')
			.end(function (err, res) {
				// If something went wrong, bail
				if (!res.ok) {
					console.warn('Error retrieving CSRF token:', err);
					return;
				}
				// Set global CSRF token
				thumbsup.csrfToken = res.body;
				// Run callback
				callback();
			})
		;
	},
	// Cast vote
	_vote: function (elementId, key, vote) {
		// If setup is not complete, bail
		if (!this.setupComplete) {
			return;
		}
		// Make object available to callback
		const that = this;
		// Callback function for casting a vote
		const castVote = function () {
			// Initialize data with CSRF token
			const data = JSON.parse(JSON.stringify(that.csrfToken));
			// Set data
			data['id'] = elementId;
			data['key'] = key;
			// Set vote icons
			const voteIcons = Sizzle('.thumbsup-' + vote + '-' + that._setItemKey(elementId, key));
			const voteMatch = that._determineMatch(voteIcons);
			// If matching vote has not been cast
			if (!voteMatch) {
				// Define opposite
				const opposite = vote === 'upvote' ? 'downvote' : 'upvote';

				// Set opposite icons
				const oppositeIcons = Sizzle('.thumbsup-' + opposite + '-' + that._setItemKey(elementId, key));
				const oppositeMatch = that._determineMatch(oppositeIcons);
				let action = that.getActionUrl() + '/thumbsup/vote/';
				// If opposite vote has already been cast
				if (oppositeMatch) {
					action += 'swap'; // Swap vote
				} else {
					action += vote; // Cast new vote
				}
				// Vote via AJAX
				ajax
					.post(action)
					.send(data)
					.type('form')
					.set('X-Requested-With', 'XMLHttpRequest')
					.end(function (response) {
						// Get entry data
						const entry = JSON.parse(response.text);
						// Set message prefix
						const prefix = '[' + entry.id + ']' + (entry.key ? ' [' + entry.key + ']' : '');
						// If error was returned, log and bail
						if (typeof entry === 'string') {
							console.warn(prefix + ' ' + entry);
							return;
						}
						// If swapping vote
						if (oppositeMatch) {
							entry.userVote = entry.userVote * 2;
							thumbsup._removeMatchClass(oppositeIcons);
						}
						// Update values & add class
						thumbsup._setAllValues(entry);
						thumbsup._addMatchClass(voteIcons);
					})
				;
			} else {
				// Unvote
				thumbsup.removeVote(elementId, key);
			}
		};
		// If token already exists
		if (this.csrfToken) {
			// Cast vote using existing token
			castVote();
		} else {
			// Cast vote using a fresh token
			this.getCsrf(castVote);
		}
	},
	// Update all numeric values on the page
	_setAllValues: function (entry) {
		thumbsup._setValue(entry, 'tally', 'tally');
		thumbsup._setValue(entry, 'totalVotes', 'total-votes');
		thumbsup._setValue(entry, 'totalUpvotes', 'total-upvotes');
		thumbsup._setValue(entry, 'totalDownvotes', 'total-downvotes');
	},
	// Update the numeric value for a single element
	_setValue: function (entry, key, classSuffix) {
		// Collect matching DOM elements
		const group = "[data-id='" + entry['itemKey'] + "']";

		const results = document.querySelectorAll(group + ".thumbsup-" + classSuffix);
		// Set value for all matching elements
		for (let el of results) {
			el.innerHTML = parseInt(entry[key]);
		}
	},
	// Generate combined item key
	_setItemKey: function (elementId, key) {
		return elementId + (key ? '-' + key : '');
	},
	// Determine whether matching vote has already been cast
	_determineMatch: function (icons) {
		if (!icons.length) {
			return false;
		} else {
			return ((' ' + icons[0].className + ' ').indexOf(' thumbsup-active ') > -1);
		}
	},
	// Add vote match class to icons
	_addMatchClass: function (icons) {
		for (let i = 0; i < icons.length; i++) {
			icons[i].className += ' thumbsup-active';
			icons[i].closest('.js-thumbsup-container').className += ' thumbsup-voted';
		}
	},
	// Remove vote match class from icons
	_removeMatchClass: function (icons) {
		for (let i = 0; i < icons.length; i++) {
			icons[i].className = icons[i].className.replace('thumbsup-active', '');
			let parent = icons[i].closest('.js-thumbsup-container');
			parent.className = parent.className.replace('thumbsup-voted', '');
		}
	},
	// Check whether a DOM element has specified class
	// _hasClass(element, className) {
	// 	return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
	// }
};

// On page load, optionally preload Upvote data
addEventListener('load', function () {
	// Determine whether to preload data
	const checkPreloadSetting = function () {
		// Initialize data with CSRF token
		const data = JSON.parse(JSON.stringify(thumbsup.csrfToken));
		// Remove vote
		ajax
			.post(thumbsup.getActionUrl() + '/thumbsup/page/preload')
			.send(data)
			.end(function (err, res) {
				// If something went wrong, bail
				if (!res.ok) {
					console.warn('Could not determine whether ThumbsUp should be preloaded:', err);
					return;
				}
				// Get response data
				const data = res.body;
				// If setting is not enabled, bail (successfully)
				if (!data || !data.enabled) {
					return;
				}
				// Set up all Upvote elements on the page
				thumbsup.pageSetup();
			})
		;
	};
	// If token already exists
	if (thumbsup.csrfToken) {
		// Check whether to preload data using existing token
		checkPreloadSetting();
	} else {
		// Check whether to preload data using a fresh token
		thumbsup.getCsrf(checkPreloadSetting);
	}
});
