// This script is only available if "Allow vote removal" is checked

// Extend thumbsup object to allow vote removal
thumbsup.removeVote = function (elementId, key) {
	// If setup is not complete, bail
	if (!thumbsup.setupComplete) {
		return;
	}
	// Callback function for casting a vote
	const removeVote = function () {
		// Initialize data with CSRF token
		const data = JSON.parse(JSON.stringify(thumbsup.csrfToken));
		// Set data
		data['id'] = elementId;
		data['key'] = key;
		// Remove vote
		ajax
			.post(thumbsup.getActionUrl() + '/thumbsup/vote/remove')
			.send(data)
			.type('form')
			.set('X-Requested-With', 'XMLHttpRequest')
			.end(function (response) {
				const entry = JSON.parse(response.text);
				// If error was returned, log and bail
				if (typeof entry === 'string') {
					console.warn(prefix + ' ' + entry);
					return;
				}
				// Update values & remove classes
				thumbsup._setAllValues(entry);
				thumbsup._removeVoteClass(entry.id, entry.key, 'upvote');
				thumbsup._removeVoteClass(entry.id, entry.key, 'downvote');
			})
		;
	};
	// If token already exists
	if (thumbsup.csrfToken) {
		// Cast vote using existing token
		removeVote();
	} else {
		// Cast vote using a fresh token
		thumbsup._csrf(removeVote);
	}
};

// Extend thumbsup object to allow vote removal
thumbsup._removeVoteClass = function (elementId, key, vote) {
	const icons = Sizzle('.thumbsup-' + vote + '-' + thumbsup._setItemKey(elementId, key));
	thumbsup._removeMatchClass(icons);
};
