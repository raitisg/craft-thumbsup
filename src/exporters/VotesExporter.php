<?php

namespace raitisg\thumbsup\exporters;

use Craft;
use craft\base\ElementExporter;
use craft\elements\db\ElementQueryInterface;

class VotesExporter extends ElementExporter
{
	public static function displayName(): string {
		return Craft::t('thumbsup', 'Votes table (default)');
	}

	public function export(ElementQueryInterface $query): array {
		$results = [];

		foreach ($query->each() as $element) {
			$results[] = [
				'Title' => $element->title,
				'Revision' => $element->revision,
				'Post date' => $element->postDate,
				'Total' => $element->total,
				'Upvotes' => $element->upvotes,
				'Downvotes' => $element->downvotes,
				'Tally' => $element->tally,
				'URL' => $element->getUrl(),
			];
		}

		return $results;
	}
}
