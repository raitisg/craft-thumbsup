<?php

namespace raitisg\thumbsup\migrations;

use craft\db\Migration;

class Install extends Migration
{
	public function safeUp(): void {
		$this->createTable('{{%thumbsup_totals}}', [
			'id' => $this->integer(),
			'key' => $this->integer(),
			'upvotes' => $this->integer()->unsigned()->defaultValue(0),
			'downvotes' => $this->integer()->unsigned()->defaultValue(0),
			'total' => $this->integer()->unsigned()->defaultValue(0),
			'tally' => $this->integer()->defaultValue(0),
			'uid' => $this->uid(),
			'dateUpdated' => $this->dateTime()->notNull(),
			'dateCreated' => $this->dateTime()->notNull(),
		]);

		$this->createTable('{{%thumbsup_votes}}', [
			'id' => $this->integer(),
			'key' => $this->integer(),
			'userHash' => $this->char(40),
			'vote' => $this->tinyInteger(),
			'uid' => $this->uid(),
			'dateUpdated' => $this->dateTime()->notNull(),
			'dateCreated' => $this->dateTime()->notNull(),
		]);

		$this->addPrimaryKey('id_key', '{{%thumbsup_totals}}', ['id', 'key']);

		$this->addForeignKey(null, '{{%thumbsup_totals}}', ['id'], '{{%elements}}', ['id'], 'CASCADE');
		$this->addForeignKey(null, '{{%thumbsup_totals}}', ['key'], '{{%revisions}}', ['id'], 'CASCADE');

		$this->addForeignKey(null, '{{%thumbsup_votes}}', ['id'], '{{%elements}}', ['id'], 'CASCADE');
		$this->addForeignKey(null, '{{%thumbsup_votes}}', ['key'], '{{%revisions}}', ['id'], 'CASCADE');
	}

	public function safeDown(): void {
		$this->dropTableIfExists('{{%thumbsup_totals}}');
		$this->dropTableIfExists('{{%thumbsup_votes}}');
	}
}
