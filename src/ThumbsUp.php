<?php

namespace raitisg\thumbsup;

use Craft;
use craft\base\Plugin;
use craft\web\twig\variables\CraftVariable;
use raitisg\thumbsup\models\Settings;
use raitisg\thumbsup\services\Query;
use raitisg\thumbsup\services\ThumbsUpService;
use raitisg\thumbsup\services\Vote;
use raitisg\thumbsup\variables\ThumbsUpVariable;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use yii\base\Event;
use yii\base\Exception;

class ThumbsUp extends Plugin
{
	public const UPVOTE = +1;
	public const DOWNVOTE = -1;

	/** @var Plugin $plugin */
	public static $plugin;

	/** @var bool $hasCpSettings */
	public bool $hasCpSettings = true;

	/** @var bool $hasCpSection */
	public bool $hasCpSection = true;

	/** @var bool $schemaVersion */
	public string $schemaVersion = '1.0.0';

	/**
	 * @return void
	 */
	public function init(): void {
		parent::init();
		self::$plugin = $this;

		$this->name = 'FAQ Ratings';

		$this->setComponents([
			'thumbsup' => ThumbsUpService::class,
			'thumbsup_query' => Query::class,
			'thumbsup_vote' => Vote::class,
		]);

		$this->thumbsup->getVotes();

		// Register variables
		Event::on(
			CraftVariable::class,
			CraftVariable::EVENT_INIT,
			function (Event $event) {
				$variable = $event->sender;
				$variable->set('thumbsup', ThumbsUpVariable::class);
			}
		);
	}

	/**
	 * @return Settings
	 */
	protected function createSettingsModel(): Settings {
		return new Settings();
	}

	/**
	 * @return string The rendered settings HTML
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 * @throws Exception
	 */
	protected function settingsHtml(): string {
		return Craft::$app->getView()->renderTemplate(
			'thumbsup/settings',
			[
				'settings' => $this->getSettings()
			]
		);
	}
}
